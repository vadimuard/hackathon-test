const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const app = express();
const mustache = require('mustache-express');

//routes
const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.set("view engine", 'mst');
const VIEWS_PATH = path.join(__dirname, "/views");
app.set('views', VIEWS_PATH);
app.engine('mst', mustache(path.join(VIEWS_PATH, '/partials')));

app.use('/', indexRouter);
app.use('/api',apiRouter);

module.exports = app;