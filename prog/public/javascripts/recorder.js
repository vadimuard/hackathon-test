const getAudio = function (stream) {
    // var request = require("request");
    let chunks = [];
    const startRec = document.getElementById('start');
    const stopRec = document.getElementById('stop');
    const mediaRecorder = new MediaRecorder(stream);
    startRec.addEventListener('click', () => {
        mediaRecorder.start();
        stopRec.disabled = false;
        startRec.disabled = true;
        chunks = [];
    });
    stopRec.addEventListener('click', () => {
        mediaRecorder.stop();
        stopRec.disabled = true;
        startRec.disabled = false;
    });
    mediaRecorder.addEventListener('dataavailable', ev => {
        chunks.push(ev.data);
    })
    mediaRecorder.addEventListener('stop', ev => {
        const blob = new Blob(chunks, { 'type': 'audio/ogg; codecs=opus' });
        const audioSrc = URL.createObjectURL(blob);
        var data = {
            'file': blob,
            'url': audioSrc,
            'return': 'deezer',
            'api_token': 'd87eca7bd85fa66e556667cef8c931a5'
        }
        const formData = new FormData();
        formData.append('url', audioSrc);
        formData.append('return', 'deezer');
        formData.append('api_token', 'd87eca7bd85fa66e556667cef8c931a5');
        formData.append('file',blob);
        fetch('api/song/search', { method: 'POST', body: formData }).then(() => { console.log('recorder.js') })
    })
}