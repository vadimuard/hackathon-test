const startGame = document.getElementById('start-game');
const mainDiv = document.getElementById('main');

startGame.addEventListener('click', () => {
    fetch('/templates/inputs.mst')
        .then(x => x = x.text())
        .then(template => {
            mainDiv.innerHTML = template;
            navigator.mediaDevices.getUserMedia({ audio: true, video: false })
                .then(getAudio);
        })
})